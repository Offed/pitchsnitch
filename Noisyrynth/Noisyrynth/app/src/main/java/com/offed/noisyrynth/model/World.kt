package com.example.pitchmaze.model

import android.util.Log
import com.example.pitchmaze.model.extensions.at
import com.example.pitchmaze.model.extensions.addAtInnerList
import com.example.pitchmaze.model.extensions.dimensions
import com.example.pitchmaze.model.extensions.removeAtInnerList
import com.example.pitchmaze.model.system.IVector2
import com.example.pitchmaze.model.system.Vector2i
import com.offed.noisyrynth.model.system.events.EventBus
import com.offed.noisyrynth.model.system.events.events.GameFinishedEvent

class World (private val board : MutableList<MutableList<Char>>, val gameEventBus : EventBus) : IWorld {
    private var playerPosition : Vector2i
    override val worldSize: IVector2<Int>

    init {
        var x = -1
        val y = board.indexOfFirst {
            x = it.indexOfFirst{
                it == '@'
            }
            x != -1
        }
        playerPosition = Vector2i(x, y)

        worldSize = board.dimensions()
    }

    override fun getStateAt(position : IVector2<Int>) : WorldFieldType {
        val field = board.at(position)
        return when (field) {
            PLAYER_SYMBOL -> WorldFieldType.Player
            EMPTY_SYMBOL -> WorldFieldType.Empty
            WALL_SYMBOL -> WorldFieldType.Wall
            FINISH_SYMBOL -> WorldFieldType.Finish
            else -> WorldFieldType.Unknown
        }
    }

    fun movePlayer (direction : Direction) {
        val move = when (direction) {
            Direction.Down -> Vector2i(0, 1)
            Direction.Left -> Vector2i(-1, 0)
            Direction.Right -> Vector2i(1, 0)
            Direction.Top -> Vector2i(0,-1)
        }

        val newPosition = playerPosition + move

        when (board.at(newPosition)) {
            EMPTY_SYMBOL -> changePlayerPositionOnBoard(newPosition)
            FINISH_SYMBOL -> finishGame()
            else -> {/*do nothing*/}
        }
    }

    private fun changePlayerPositionOnBoard(position : IVector2<Int>) {
        board.removeAtInnerList(playerPosition)
        board.addAtInnerList(playerPosition, EMPTY_SYMBOL)
        board.removeAtInnerList(position)
        board.addAtInnerList(position, PLAYER_SYMBOL)
        playerPosition = position as Vector2i
    }

    private fun finishGame() {
        gameEventBus.publish(GameFinishedEvent())
    }

    companion object {
        const val PLAYER_SYMBOL = '@'
        const val WALL_SYMBOL = '#'
        const val EMPTY_SYMBOL = ' '
        const val FINISH_SYMBOL = 'O'
    }
}