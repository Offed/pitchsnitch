package com.offed.noisyrynth.view.nodes

import android.graphics.Canvas

interface IDrawable {
    fun draw(canvas : Canvas)
}