package com.offed.noisyrynth.view.nodes

import com.example.pitchmaze.model.system.Vector2i

interface ITransformable {
    var position : Vector2i
    var rotation : Float
}