package com.offed.noisyrynth.soundClassification

import com.example.pitchmaze.model.Direction

/**
 * Created by Offed on 2017-10-08.
 */
class keySound(val soundName: String, val keyDirection : Direction, val soundFrequency : Int) {

    override fun toString(): String {
        return soundMarker
    }


    val soundMarker: String
        get() = "<" + soundName!!.toLowerCase().replace(" ", "_") + ">"

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val keySound =
            o as keySound
        if (soundFrequency != keySound.soundFrequency) return false
        return if (soundName != null) soundName == keySound.soundName else keySound.soundName == null
    }

    override fun hashCode(): Int {
        var result = if (soundName != null) soundName.hashCode() else 0
        result = 31 * result + soundFrequency
        return result
    }


}