package com.offed.noisyrynth.view.nodes

interface IViewEntity : ITransformable, IDrawable