package com.offed.noisyrynth.view

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.example.pitchmaze.model.system.Vector2i
import com.offed.noisyrynth.view.nodes.Arrow
import com.offed.noisyrynth.view.nodes.IViewEntity

class DirectionView(private val size : Int) : IViewEntity{
    override var position = Vector2i(0, 0)
    override var rotation = 0.0f
    var color = Color.WHITE

    override fun draw(canvas: Canvas) {
        val paint = Paint()
        val arrow = Arrow(size)

        paint.color = color
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.strokeWidth = 8f
        paint.isAntiAlias = true

        canvas.drawRect(
            position.x.toFloat(),
            position.y.toFloat(),
            (position.x + size).toFloat(),
            (position.y + size).toFloat(),
            paint
        )

        arrow.position = position
        arrow.rotation = rotation
        arrow.draw(canvas)

    }
}