package com.offed.noisyrynth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import be.tarsos.dsp.AudioDispatcher
import be.tarsos.dsp.AudioProcessor
import be.tarsos.dsp.io.android.AudioDispatcherFactory
import be.tarsos.dsp.pitch.PitchDetectionHandler
import be.tarsos.dsp.pitch.PitchProcessor
import com.example.pitchmaze.model.Direction
import com.example.pitchmaze.model.World
import com.example.pitchmaze.view.ViewDrawer
import com.offed.noisyrynth.model.system.events.EventBus
import com.offed.noisyrynth.model.system.events.IEventListener
import com.offed.noisyrynth.model.system.events.events.GameFinishedEvent
import com.offed.noisyrynth.soundClassification.deviceModel
import com.offed.noisyrynth.soundClassification.keySound
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import kotlin.math.roundToInt

class pitchMazeActivity : AppCompatActivity(), IEventListener<GameFinishedEvent> {
    //tarsos/pitch detection objects
    var dispatcher: AudioDispatcher? = null
    var pdh: PitchDetectionHandler? = null
    var audioThread: Thread? = null
    var pitchProcessor: AudioProcessor? = null
    //helper variables for the activity
    private var active = false
    //device model to be used in detection
    private var device: deviceModel? = null
    //views
    var viewDrawer = ViewDrawer()
    val gameEventBus = EventBus()
    lateinit var pitchView: TextView
    lateinit var tipView: TextView
    lateinit var world: World

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pitch_maze)

        var selectedDeviceFilename = this.getIntent().getStringExtra("deviceFileName")
        setupPitchListener(selectedDeviceFilename)

        world = World(device!!.gameMap.toMutableList(), gameEventBus)
        gameEventBus.subscribe(this)
        viewDrawer.worldView = findViewById(R.id.imageView)
        viewDrawer.directionsView = findViewById(R.id.pitchDirectionView)
        tipView = findViewById(R.id.tipTextView)

        viewDrawer.drawWorld(world)
        pitchView = findViewById(R.id.pitchView)
    }

    fun processPitch(pitchInHz: Float) {
        pitchView.text = "" + pitchInHz

        var tipString = ""
        if (pitchInHz == -1.0f) {
            tipString = "Make a sound!"
            viewDrawer.drawDirections()
        }
        else {
            val leftDifference = (device!!.keySounds.find { it.keyDirection == Direction.Left }!!.soundFrequency - pitchInHz).roundToInt()
            val upDifference = (device!!.keySounds.find { it.keyDirection == Direction.Top }!!.soundFrequency - pitchInHz).roundToInt()
            val downDifference = (device!!.keySounds.find { it.keyDirection == Direction.Down }!!.soundFrequency - pitchInHz).roundToInt()
            val rightDifference = (device!!.keySounds.find { it.keyDirection == Direction.Right }!!.soundFrequency - pitchInHz).roundToInt()

            tipString += "Left: " + leftDifference
            tipString += " Up: " + upDifference
            tipString += " Down: " + downDifference
            tipString += " Right: " + rightDifference

            viewDrawer.drawDirections(
                arrayOf(
                    rightDifference,
                    leftDifference,
                    downDifference,
                    upDifference
                )
            )
        }
        tipView.text = tipString

        //sound matching part
        val detectedKey: keySound? = device!!.matchFrequencyToKey(pitchInHz)



        if (detectedKey != null) {

            world.movePlayer(detectedKey.keyDirection)
            viewDrawer.drawWorld(world)


            val marker: String = detectedKey.soundMarker
            Log.i(LOG_TAG, "Sound detected: $marker")
        }
    }

    override fun receive(event: GameFinishedEvent) {
        Toast.makeText(applicationContext, R.string.game_finished, Toast.LENGTH_LONG).show()
        world = World(device!!.gameMap.toMutableList(), gameEventBus)
    }

    private fun setupPitchListener(selectDeviceActivity: String) {
        try {
            device = setDeviceModel(selectDeviceActivity)
            viewDrawer.keySounds = device!!.keySounds
        } catch (e: IOException) {
            e.printStackTrace()
        }
        pdh = PitchDetectionHandler { pitchDetectionResult, audioEvent ->
            val pitchInHz = pitchDetectionResult.pitch
            runOnUiThread(Runnable { processPitch(pitchInHz) })
        }
        val sampleRate = UtilityClass.maximumSupportedSampleRate
        Log.i(
            LOG_TAG,
            "Detected supported sample rate of $sampleRate"
        )
        val bufferSize: Int
        bufferSize = if (sampleRate > 22050) {
            2048
        } else {
            1024
        }
        pitchProcessor = PitchProcessor(
            PitchProcessor.PitchEstimationAlgorithm.FFT_YIN,
            sampleRate.toFloat(),
            bufferSize,
            pdh
        )
        //pitchProcessor = new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.MPM, sampleRate, bufferSize, pdh);
        dispatcher = AudioDispatcherFactory.fromDefaultMicrophone(
            sampleRate,
            bufferSize,
            0
        )
        dispatcher!!.addAudioProcessor(pitchProcessor)
        audioThread = Thread(dispatcher, "Audio thread (pitch)")
        audioThread!!.start()
        active = true
    }

    private fun killPitchListener() {
        if (dispatcher != null) {
            dispatcher!!.stop()
            dispatcher = null
        }
        pdh = null
        pitchProcessor = null
        active = false
    }

    @Throws(IOException::class)
    private fun setDeviceModel(deviceName: String): deviceModel {
        Log.i("SET_DEVICE_MODEL", "Got filename:${deviceName}")
        Log.i(
            LOG_TAG,
            "Expected device file at " + UtilityClass.structureName + "/" + deviceName
        )
        val file =
            File(UtilityClass.structureName + "/" + deviceName)
        if (file.exists()) {
            Log.i(
                LOG_TAG,
                "$deviceName OK"
            )
        }
        val iStream: InputStream = FileInputStream(file)
        val writer: Writer = StringWriter()
        val buffer = CharArray(iStream.available())
        try {
            val reader: Reader =
                BufferedReader(InputStreamReader(iStream, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
            finish()
        } finally {
            iStream.close()
        }
        val jsonString = String(buffer)
        var deviceJsonObject: JSONObject? = null
        try {
            deviceJsonObject = JSONObject(jsonString)
        } catch (e: JSONException) {
            e.printStackTrace()
            finish()
        }
        return deviceModel(deviceJsonObject!!)
    }

    override fun onStop() {
        super.onStop()
        killPitchListener()
    }

    override fun onResume() {
        super.onResume()
        if (!active) {
            setupPitchListener(this.getIntent().getStringExtra("deviceFileName"))
        }
    }

    override fun onPause() {
        super.onPause()
        if (active) {
            killPitchListener()
        }
    }

    companion object {
        const val LOG_TAG = "PitchMazeActivity"
    }
}
