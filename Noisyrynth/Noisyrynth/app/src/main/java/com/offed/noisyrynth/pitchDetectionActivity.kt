package com.offed.noisyrynth

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import be.tarsos.dsp.AudioDispatcher
import be.tarsos.dsp.AudioProcessor
import be.tarsos.dsp.io.android.AudioDispatcherFactory
import be.tarsos.dsp.pitch.PitchDetectionHandler
import be.tarsos.dsp.pitch.PitchProcessor
import com.offed.noisyrynth.UtilityClass.maximumSupportedSampleRate
import com.offed.noisyrynth.soundClassification.deviceModel
import com.offed.noisyrynth.soundClassification.keySound
import org.json.JSONException
import org.json.JSONObject
import java.io.*

class pitchDetectionActivity : AppCompatActivity() {
    //tarsos/pitch detection objects
    var dispatcher: AudioDispatcher? = null
    var pdh: PitchDetectionHandler? = null
    var audioThread: Thread? = null
    var pitchProcessor: AudioProcessor? = null
    //views in the activity
    var pitchDisplay: TextView? = null
    var stateChanges: TextView? = null
    var detectedKeys: TextView? = null
    var deviceName: TextView? = null
    //helper variables for the activity
    private var stateCounter = 0
    private var active = false
    private var selectedDeviceFilename: String? = null
    //device model to be used in detection
    private var device: deviceModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pitch_detection)
        pitchDisplay = findViewById(R.id.pitchDisplay) as TextView?
        stateChanges = findViewById(R.id.stateChanges) as TextView?
        detectedKeys = findViewById(R.id.keysDetected) as TextView?
        deviceName = findViewById(R.id.deviceName) as TextView?
        selectedDeviceFilename = this.getIntent().getStringExtra("deviceFileName")
        setupPitchListener()
    }

    fun processPitch(pitchInHz: Float) {
        pitchDisplay!!.text = "" + pitchInHz
        stateCounter++
        stateChanges!!.text = "" + stateCounter
        //sound matching part
        val detectedKey: keySound? = device!!.matchFrequencyToKey(pitchInHz)

        if (detectedKey != null) {
            val marker: String = detectedKey.soundMarker
            detectedKeys!!.append(marker)
            if (detectedKeys!!.length() > 300) detectedKeys!!.text = ""
        }
    }

    private fun setupPitchListener() {
        try {
            device = setDeviceModel()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        pdh = PitchDetectionHandler { pitchDetectionResult, audioEvent ->
            val pitchInHz = pitchDetectionResult.pitch
            runOnUiThread(Runnable { processPitch(pitchInHz) })
        }
        val sampleRate = maximumSupportedSampleRate
        Log.i(
            LOG_TAG,
            "Detected supported sample rate of $sampleRate"
        )
        val bufferSize: Int
        bufferSize = if (sampleRate > 22050) {
            2048
        } else {
            1024
        }
        pitchProcessor = PitchProcessor(
            PitchProcessor.PitchEstimationAlgorithm.FFT_YIN,
            sampleRate.toFloat(),
            bufferSize,
            pdh
        )
        //pitchProcessor = new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.MPM, sampleRate, bufferSize, pdh);
        dispatcher = AudioDispatcherFactory.fromDefaultMicrophone(
            sampleRate,
            bufferSize,
            0
        )
        dispatcher!!.addAudioProcessor(pitchProcessor)
        audioThread = Thread(dispatcher, "Audio thread (pitch)")
        audioThread!!.start()
        active = true
    }

    private fun killPitchListener() {
        if (dispatcher != null) {
            dispatcher!!.stop()
            dispatcher = null
        }
        pdh = null
        pitchProcessor = null
        active = false
    }

    @Throws(IOException::class)
    private fun setDeviceModel(): deviceModel {
        Log.i("SET_DEVICE_MODEL", "Got filename:$selectedDeviceFilename")
        Log.i(
            LOG_TAG,
            "Expected device file at " + UtilityClass.structureName + "/" + selectedDeviceFilename
        )
        val file =
            File(UtilityClass.structureName + "/" + selectedDeviceFilename)
        if (file.exists()) {
            Log.i(
                LOG_TAG,
                "$selectedDeviceFilename OK"
            )
        }
        val iStream: InputStream = FileInputStream(file)
        val writer: Writer = StringWriter()
        val buffer = CharArray(iStream.available())
        try {
            val reader: Reader =
                BufferedReader(InputStreamReader(iStream, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
            finish()
        } finally {
            iStream.close()
        }
        val jsonString = String(buffer)
        var deviceJsonObject: JSONObject? = null
        try {
            deviceJsonObject = JSONObject(jsonString)
        } catch (e: JSONException) {
            e.printStackTrace()
            finish()
        }
        return deviceModel(deviceJsonObject!!)
    }

    override fun onStop() {
        super.onStop()
        killPitchListener()
    }

    override fun onResume() {
        super.onResume()
        if (!active) {
            setupPitchListener()
        }
    }

    override fun onPause() {
        super.onPause()
        if (active) {
            killPitchListener()
        }
    }

    companion object {
        const val LOG_TAG = "PitchDetectionActivity"
    }
}