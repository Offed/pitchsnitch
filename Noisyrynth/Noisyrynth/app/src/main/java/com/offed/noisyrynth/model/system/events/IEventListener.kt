package com.offed.noisyrynth.model.system.events

interface IEventListener <T> {
    fun receive(event : T)
}

inline fun <reified T> IEventListener<T>.getListeningTypeName() : String {
    return T::class.simpleName!!
}