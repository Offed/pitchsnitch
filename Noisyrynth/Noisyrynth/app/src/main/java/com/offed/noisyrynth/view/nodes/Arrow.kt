package com.offed.noisyrynth.view.nodes

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.example.pitchmaze.model.system.Vector2i

class Arrow(private val size : Int) : IViewEntity {
    override var position = Vector2i(0, 0)
    override var rotation = 0.0f

    override fun draw(canvas: Canvas) {
        val paint = Paint()

        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        paint.strokeWidth = size / 10.0f
        paint.isAntiAlias = true

        val bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)
        val innerCanvas = Canvas(bitmap)

        innerCanvas.rotate(rotation, size / 2.0f, size / 2.0f)

        innerCanvas.drawLine(
            size / 2.0f ,
            0.0f ,
            size / 2.0f ,
            size.toFloat() ,
            paint
        )

        innerCanvas.drawLine(
            size / 2.0f ,
            0.0f ,
            0.0f ,
            size / 2.0f ,
            paint
        )

        innerCanvas.drawLine(
            size / 2.0f ,
            0.0f ,
            size.toFloat() ,
            size / 2.0f ,
            paint
        )

        canvas.drawBitmap(bitmap, position.x.toFloat(), position.y.toFloat(), paint)
    }
}