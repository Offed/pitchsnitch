package com.offed.noisyrynth.model.system.events

class EventBus {
    val listeners = mutableMapOf<String, MutableList<Any>>()

    inline fun <reified E> publish (event : E) {
        listeners[E::class.simpleName]?.forEach {
            @Suppress("UNCHECKED_CAST")
            (it as? IEventListener<E>)?.receive(event)
        }
    }

    inline fun <reified E, reified T : IEventListener<E>> subscribe(listener : T) {
        val typeName = listener.getListeningTypeName()

        if (listeners.keys.contains(typeName)) {
            listeners[typeName]?.add(listener)
        } else {
            @Suppress("ReplacePutWithAssignment")
            listeners.put(typeName, mutableListOf(listener))
        }
    }

    inline fun <reified E, reified T : IEventListener<E>> unsubscribe(listener : T) {
        listeners[listener.getListeningTypeName()]?.remove(listener)
    }
}