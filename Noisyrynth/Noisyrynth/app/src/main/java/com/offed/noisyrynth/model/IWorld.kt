package com.example.pitchmaze.model

import com.example.pitchmaze.model.system.IVector2

interface IWorld {
    val worldSize : IVector2<Int>
    fun getStateAt(position: IVector2<Int>): WorldFieldType
}