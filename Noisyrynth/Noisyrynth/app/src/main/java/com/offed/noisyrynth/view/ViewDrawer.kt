package com.example.pitchmaze.view

import android.graphics.*
import android.widget.ImageView
import com.example.pitchmaze.model.IWorld
import com.example.pitchmaze.model.WorldFieldType
import com.example.pitchmaze.model.system.Vector2i
import com.offed.noisyrynth.soundClassification.keySound
import com.offed.noisyrynth.view.DirectionView
import com.offed.noisyrynth.view.nodes.Arrow
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class ViewDrawer {
    lateinit var worldView : ImageView
    lateinit var directionsView : ImageView
    lateinit var keySounds : ArrayList<keySound>
    val paint = Paint()

    fun drawWorld(world : IWorld) {
        val worldSize = world.worldSize
        val canvasSize = Vector2i(500, 500)
        val fieldSize = (canvasSize / worldSize) as Vector2i

        val fieldList = listOf(
            FieldView(fieldSize, WorldFieldType.Player),
            FieldView(fieldSize, WorldFieldType.Wall),
            FieldView(fieldSize, WorldFieldType.Empty),
            FieldView(fieldSize, WorldFieldType.Finish),
            FieldView(fieldSize, WorldFieldType.Unknown)
        )

        if (worldSize.x <= 0 || worldSize.y <= 0) {
            println("Cannot draw world with size: ${worldSize.x}x${worldSize.y}")
            return
        }

        val bitmap = Bitmap.createBitmap(500, 500, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        for (y in 0 until worldSize.x) {
            for(x in 0 until worldSize.y) {
                fieldList.first{
                    it.position = (Vector2i(x, y) * fieldSize) as Vector2i
                    it.type == world.getStateAt(Vector2i(x, y))
                }.draw(canvas)
            }
        }

//        world.getStateAt(Vector2i(3 , 3))
        worldView.setImageBitmap(bitmap)
    }

    fun drawDirections(differences: Array<Int>) {
        val directionViewSize = directionsView.width / 4
        val arrow = DirectionView(directionViewSize)

        val bitmap = Bitmap.createBitmap(directionsView.width, directionsView.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        arrow.rotation = 90.0f
        arrow.position = Vector2i(0, 0)
        arrow.color = calcDirectionColor(differences[0])
        arrow.draw(canvas)
        arrow.rotation = 270.0f
        arrow.position = Vector2i(directionViewSize, 0)
        arrow.color = calcDirectionColor(differences[1])
        arrow.draw(canvas)
        arrow.rotation = 180.0f
        arrow.position = Vector2i(directionViewSize * 2, 0)
        arrow.color = calcDirectionColor(differences[2])
        arrow.draw(canvas)
        arrow.rotation = 0.0f
        arrow.position = Vector2i(directionViewSize * 3, 0)
        arrow.color = calcDirectionColor(differences[3])
        arrow.draw(canvas)

        directionsView.setImageBitmap(bitmap)
    }

    fun drawDirections() {
        val directionViewSize = directionsView.width / 4
        val arrow = DirectionView(directionViewSize)

        val bitmap = Bitmap.createBitmap(directionsView.width, directionsView.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        arrow.rotation = 90.0f
        arrow.position = Vector2i(0, 0)
        arrow.draw(canvas)
        arrow.rotation = 270.0f
        arrow.position = Vector2i(directionViewSize, 0)
        arrow.draw(canvas)
        arrow.rotation = 180.0f
        arrow.position = Vector2i(directionViewSize * 2, 0)
        arrow.draw(canvas)
        arrow.rotation = 0.0f
        arrow.position = Vector2i(directionViewSize * 3, 0)
        arrow.draw(canvas)

        directionsView.setImageBitmap(bitmap)
    }

    private fun calcDirectionColor(difference : Int) : Int{
        val maxDifference = keySounds[0].soundFrequency - keySounds[1].soundFrequency

        val factor = 1f / if (difference != 0) abs(difference) else 1

        if (difference == 0) {
            return Color.WHITE
        } else if (difference > 0) {
            return Color.rgb( 255, (255 * factor).toInt(), (255 * factor).toInt())
        } else {
            return Color.rgb((255 * factor).toInt(), (255 * factor).toInt(), 255)
        }

    }

    companion object {
        const val defaultColor = Color.WHITE;
    }
}